// GENERATED CODE - DO NOT MODIFY BY HAND
import 'package:flutter/material.dart';
import 'package:intl/intl.dart';
import 'intl/messages_all.dart';

// **************************************************************************
// Generator: Flutter Intl IDE plugin
// Made by Localizely
// **************************************************************************

// ignore_for_file: non_constant_identifier_names, lines_longer_than_80_chars
// ignore_for_file: join_return_with_assignment, prefer_final_in_for_each
// ignore_for_file: avoid_redundant_argument_values

class S {
  S();
  
  static S current;
  
  static const AppLocalizationDelegate delegate =
    AppLocalizationDelegate();

  static Future<S> load(Locale locale) {
    final name = (locale.countryCode?.isEmpty ?? false) ? locale.languageCode : locale.toString();
    final localeName = Intl.canonicalizedLocale(name); 
    return initializeMessages(localeName).then((_) {
      Intl.defaultLocale = localeName;
      S.current = S();
      
      return S.current;
    });
  } 

  static S of(BuildContext context) {
    return Localizations.of<S>(context, S);
  }

  /// `Share`
  String get ShareButton {
    return Intl.message(
      'Share',
      name: 'ShareButton',
      desc: '',
      args: [],
    );
  }

  /// `Email`
  String get ShareButtonEmail {
    return Intl.message(
      'Email',
      name: 'ShareButtonEmail',
      desc: '',
      args: [],
    );
  }

  /// `Message`
  String get ShareButtonMessage {
    return Intl.message(
      'Message',
      name: 'ShareButtonMessage',
      desc: '',
      args: [],
    );
  }

  /// `Choose your preferred streaming music quality:`
  String get QualityTitle {
    return Intl.message(
      'Choose your preferred streaming music quality:',
      name: 'QualityTitle',
      desc: '',
      args: [],
    );
  }

  /// `High`
  String get QualityHigh {
    return Intl.message(
      'High',
      name: 'QualityHigh',
      desc: '',
      args: [],
    );
  }

  /// `Medium`
  String get QualityMedium {
    return Intl.message(
      'Medium',
      name: 'QualityMedium',
      desc: '',
      args: [],
    );
  }

  /// `Low`
  String get QualityLow {
    return Intl.message(
      'Low',
      name: 'QualityLow',
      desc: '',
      args: [],
    );
  }

  /// `Choose your playback speed:`
  String get PlayBackSpeedTitle {
    return Intl.message(
      'Choose your playback speed:',
      name: 'PlayBackSpeedTitle',
      desc: '',
      args: [],
    );
  }

  /// `Play`
  String get PlayButton {
    return Intl.message(
      'Play',
      name: 'PlayButton',
      desc: '',
      args: [],
    );
  }

  /// `Select the playlist to add to: `
  String get PlayListTitle {
    return Intl.message(
      'Select the playlist to add to: ',
      name: 'PlayListTitle',
      desc: '',
      args: [],
    );
  }
}

class AppLocalizationDelegate extends LocalizationsDelegate<S> {
  const AppLocalizationDelegate();

  List<Locale> get supportedLocales {
    return const <Locale>[
      Locale.fromSubtags(languageCode: 'en', countryCode: 'US'),
      Locale.fromSubtags(languageCode: 'de'),
      Locale.fromSubtags(languageCode: 'pt', countryCode: 'BR'),
    ];
  }

  @override
  bool isSupported(Locale locale) => _isSupported(locale);
  @override
  Future<S> load(Locale locale) => S.load(locale);
  @override
  bool shouldReload(AppLocalizationDelegate old) => false;

  bool _isSupported(Locale locale) {
    if (locale != null) {
      for (var supportedLocale in supportedLocales) {
        if (supportedLocale.languageCode == locale.languageCode) {
          return true;
        }
      }
    }
    return false;
  }
}