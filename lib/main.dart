import 'package:flutter/material.dart';
import 'Options.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:email_launcher/email_launcher.dart';
import 'package:url_launcher/url_launcher.dart';
import 'generated/l10n.dart';
import 'package:flutter_localizations/flutter_localizations.dart';
// import 'package:flutter_launch/flutter_launch.dart';


void main() => runApp(MyApp());
/// This Widget is the main application widget.
class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Flutter App',
      home: MyStatefulWidget(),
      localizationsDelegates: [
        S.delegate,
        GlobalMaterialLocalizations.delegate,
        GlobalWidgetsLocalizations.delegate,
        GlobalCupertinoLocalizations.delegate,
      ],
      supportedLocales: S.delegate.supportedLocales,
    );
  }
}


class MyStatefulWidget extends StatefulWidget {
  MyStatefulWidget({Key key}) : super(key: key);
  @override
  _MyStatefulWidgetState createState() => _MyStatefulWidgetState();

  static Widget _buildDataTable(List<Map<String, String>> input) => DataTable (
    columns: const <DataColumn>[
      DataColumn(
        label: Text(
          'No',
          style: TextStyle(fontStyle: FontStyle.italic),
        ),
      ),
      DataColumn(
        label: Text(
          'Title',
          style: TextStyle(fontStyle: FontStyle.italic),
        ),
      ),
      DataColumn(
        label: Text(
          'Length',
          style: TextStyle(fontStyle: FontStyle.italic),
        ),
      ),
      DataColumn(
        label: Text(
          '',
          style: TextStyle(fontStyle: FontStyle.italic),
        ),
      ),
    ],
    rows:
    input.map(
      ((element) => DataRow(
        cells: <DataCell>[
          DataCell(Text(element["No"])), //Extracting from Map element the value
          DataCell(Text(element["Title"])),
          DataCell(Text(element["Length"])),
          DataCell(ActionChip(label: Text("Play"), onPressed: () {}, backgroundColor: Colors.blue,))
        ],
      )),
    ).toList(),
  );
}
class _MyStatefulWidgetState extends State<MyStatefulWidget> {
  var arr = ['caribou.jpg','okaw.jpg','Ram.jpg','gf.png'];
  final GlobalKey<ScaffoldState> _scaffoldKey = GlobalKey<ScaffoldState>();

  Widget _buildDecoratedImage(int imageIndex) => Expanded(
    child: Card(
      child: InkWell(
        splashColor: Colors.blue.withAlpha(30),
        onTap: () {
          setState(() {
            _selectedIndex = imageIndex;
          });
        },
        child: Container(
          width: 300,
          height: 200,
          child: Image.asset('assets/images/' + arr[imageIndex]),
        ),
      ),
      margin: const EdgeInsets.all(4),
    ),
  );
  Widget _buildImageRow(int imageIndex) => Row(
    children: [
      _buildDecoratedImage(imageIndex),
      _buildDecoratedImage(imageIndex+1),
    ],
  );

  Widget _buildImageColumn() => Container(
    decoration: BoxDecoration(
      color: Colors.white,
    ),
    child: Column(
      children: [
        _buildImageRow(0),
        _buildImageRow(2),
      ],
    ),
  );
  int _selectedIndex = 0;
  List<Widget> _widgetOptions = <Widget>[
    Container(
      //Caribou Suddenly Track List
      child: MyStatefulWidget._buildDataTable([
        {"No": "1", "Title": "Sister", "Length": "2:11"},
        {"No": "2", "Title": "You and I", "Length": "4:03"},
        {"No": "3", "Title": "Sunny's Time", "Length": "2:49"},
        {"No": "4", "Title": "New Jade", "Length": "3:37"},
        {"No": "5", "Title": "Home", "Length": "2:36"},
        {"No": "6", "Title": "Lime", "Length": "2:55"},
        {"No": "7", "Title": "Never Come Back", "Length": "5:05"},
        {"No": "8", "Title": "Filtered Grand Piano", "Length": "0:53"},
        {"No": "9", "Title": "Like I Loved You", "Length": "4:05"},
        {"No": "10", "Title": "Magpie", "Length": "3:55"},
        {"No": "11", "Title": "Ravi", "Length": "4:29"},
        {"No": "12", "Title": "Cloud Song", "Length": "6:50"},
      ]),
    ),
    Container(
      //DJ Okawari - Compass Track List
      child: MyStatefulWidget._buildDataTable([
        {"No": "1", "Title": "Bounce feat. Talib Kweli", "Length": "2:20"},
        {"No": "2", "Title": "Starry Sky", "Length": "4:25"},
        {"No": "3", "Title": "Midnight Train feat. Emi Meyer", "Length": "5:05"},
        {"No": "4", "Title": "Yours", "Length": "4:01"},
        {"No": "5", "Title": "Keep Falling feat. GIOVANCA", "Length": "3:36"},
        {"No": "6", "Title": "Last Note", "Length": "2:21"},
        {"No": "7", "Title": "Eventually feat. Emily Styler", "Length": "3:40"},
        {"No": "8", "Title": "Mirage ", "Length": "4:21"},
        {"No": "9", "Title": "Be There feat. Brittany Campbell", "Length": "5:32"},
        {"No": "10", "Title": "SADAME ", "Length": "4:03"},
        {"No": "11", "Title": "Four Water", "Length": "4:29"},
      ]),
    ),
    Container(
      //Daft Punk Random Access Memories Track List
      child: MyStatefulWidget._buildDataTable([
        {"No": "1", "Title": "Give Life Back to Music", "Length": "4:34"},
        {"No": "2", "Title": "The Game of Love", "Length": "5:21"},
        {"No": "3", "Title": "Giorgio by Moroder", "Length": "9:04"},
        {"No": "4", "Title": "Within", "Length": "3:48"},
        {"No": "5", "Title": "Instant Crush  feat. Julian Casablancas", "Length": "5:37"},
        {"No": "6", "Title": "Lose Yourself to dance feat. Pharrel Williams", "Length": "5:53"},
        {"No": "7", "Title": "Touch feat. Paul Williams", "Length": "8:18"},
        {"No": "8", "Title": "Get Lucky feat. Pharell Williams ", "Length": "6:08"},
        {"No": "9", "Title": "Beyond", "Length": "4:50"},
        {"No": "10", "Title": "Motherboard ", "Length": "5:41"},
        {"No": "11", "Title": "Fragments of Time feat. Todd Edwards", "Length": "4:39"},
        {"No": "12", "Title": "Doin' It Right feat. Panda Bear", "Length": "4:11"},
        {"No": "13", "Title": "Contact", "Length": "6:21"},
      ]),
    ),
    Container(
      //Madeon Good Faith Track List
      child: MyStatefulWidget._buildDataTable([
        {"No": "1", "Title": "Dream Dream Dream", "Length": "3:54"},
        {"No": "2", "Title": "All My Friends", "Length": "3:24"},
        {"No": "3", "Title": "Be Fine", "Length": "3:28"},
        {"No": "4", "Title": "Nirvana", "Length": "2:32"},
        {"No": "5", "Title": "Mania", "Length": "2:32"},
        {"No": "6", "Title": "Miracle", "Length": "4:10"},
        {"No": "7", "Title": "No Fear No More", "Length": "3:15"},
        {"No": "8", "Title": "Hold Me Just Because", "Length": "3:06"},
        {"No": "9", "Title": "Heavy With Hoping", "Length": "4:01"},
        {"No": "10", "Title": "Borealis ", "Length": "4:45"},
      ]),
    ),
  ];


  void popUpOptions(String option){
    if(option == Options.share){
      showModalBottomSheet<void>(
          context: context,
          builder: (BuildContext context){
            return Container(
              height: 250,
              color:Colors.blue,
              child: Column(
                mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                children:<Widget> [
                  Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children:[
                      Text(
                        "  " + S.of(context).ShareButton,
                        style: TextStyle(
                          // fontWeight: FontWeight.bold,
                          fontSize: 20,
                          color: Colors.white,
                        ),
                      ),

                      IconButton(
                        icon: const Icon(
                            Icons.close,
                            color: Colors.white
                        ),
                        onPressed: () => Navigator.pop(context),
                      ) ,


                    ],
                  ),
                  Row(
                    mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                    children: [
                      Column(
                        mainAxisAlignment: MainAxisAlignment.center,
                        children: [
                          IconButton(
                            icon: const Icon(
                                Icons.mail,
                                size: 40,
                                color:Colors.white
                            ),
                            onPressed: () async {
                              Email email = Email(
                                  to:[],
                                  cc:[],
                                  bcc:[],
                                  subject: 'Shared music',
                                  body: 'Enjoy an amazing music!!!!!!!'
                              );
                              await EmailLauncher.launch(email);
                            },

                          ),
                          Text("  " + S.of(context).ShareButtonEmail,
                            style: TextStyle(
                              color: Colors.white,
                            ),
                          ),
                        ],
                      ),
                      Column(

                        children: [
                          IconButton(
                            icon: const Icon(
                                Icons.message,
                                size: 40,
                                color:Colors.white
                            ),
                            onPressed: () async {
                              const url = 'sms:+1(650)7478374?body=Shared%20music';
                              await launch(url);
                            },
                          ),
                          Text("  " + S.of(context).ShareButtonMessage,
                            style: TextStyle(
                              color: Colors.white,
                            ),
                          ),
                        ],
                      ),
                    ],
                  ),

                ],
              ),
            );
          }
      );
    }
    else if (option == Options.quality){
      showDialog(context: context,
        child: new SimpleDialog(
          backgroundColor: Colors.blue,
          children: <Widget>[
            Text(S.of(context).QualityTitle, textAlign: TextAlign.center),
            SizedBox(height: 10,),
            SimpleDialogOption(
              onPressed: () {}, //Navigator.pop(context, Department.treasury); },
              child: Text(S.of(context).QualityHigh),
            ),
            SimpleDialogOption(
              onPressed: () {}, //Navigator.pop(context, Department.state); },
              child: Text(S.of(context).QualityMedium),
            ),
            SimpleDialogOption(
              onPressed: () {}, //Navigator.pop(context, Department.state); },
              child: Text(S.of(context).QualityLow),
            ),
          ],
        ),
      );
    }
    else if (option == Options.playback_speed){
      showDialog(context: context,
        child: new SimpleDialog(
          backgroundColor: Colors.blue,
          children: <Widget>[
            Text(S.of(context).PlayBackSpeedTitle, textAlign: TextAlign.center),
            SizedBox(height: 10,),
            SimpleDialogOption(
              onPressed: () {}, //Navigator.pop(context, Department.treasury); },
              child: Text("1x"),
            ),
            SimpleDialogOption(
              onPressed: () {}, //Navigator.pop(context, Department.state); },
              child: Text("2x"),
            ),
            SimpleDialogOption(
              onPressed: () {}, //Navigator.pop(context, Department.state); },
              child: Text("3x"),
            ),
          ],
        ),
      );
    }
    else if (option == Options.add_to_playlist){
      showDialog(context: context,
        child: new SimpleDialog(
          backgroundColor: Colors.blue,
          children: <Widget>[
            Text(S.of(context).PlayListTitle, textAlign: TextAlign.center),
            SizedBox(height: 10,),
            SimpleDialogOption(
              onPressed: () {}, //Navigator.pop(context, Department.treasury); },
              child: Text("Gym Songs"),
            ),
            SimpleDialogOption(
              onPressed: () {}, //Navigator.pop(context, Department.state); },
              child: Text("Dance Music"),
            ),
            SimpleDialogOption(
              onPressed: () {}, //Navigator.pop(context, Department.state); },
              child: Text("Relaxation Music"),
            ),
          ],
        ),
      );
    }
  }


  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Center(child: const Text('Your Muse')),
        actions: [
          Theme(
            data: Theme.of(context).copyWith(
              cardColor: Colors.blue,
            ),
            child: PopupMenuButton(
              onSelected: popUpOptions,
              itemBuilder: (BuildContext context){
                return Options.choices.map((String option){
                  return PopupMenuItem<String>(
                    value: option,
                    child: Text(option),
                  );
                }).toList();
              },
            ),
          ),
        ],
      ),
      body: ListView(
          children: [_buildImageColumn(), _widgetOptions.elementAt(_selectedIndex),]
      ),
    );
  }
}