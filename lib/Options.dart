class Options{
  static const String quality = "Quality";
  static const String playback_speed = "Playback speed";
  static const String add_to_playlist = "Add to playlist";
  static const String share = "Share";
  static const String shuffle = "Shuffle and Play";

  static const List<String> choices = <String>[
    share,
    quality,
    playback_speed,
    add_to_playlist,
    shuffle,
  ];
}
